# Filename        : run.tcl
# Description     : Script performing caribou-soi synthesis.
# Author          : Adrian Fiergolski
# Created On      : Fri Dec 9 16:35:58 2016
#
# Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2016
#
# This source file is licensed under the CERN OHL v. 1.2.
#
# You may redistribute and modify this souce file under the terms of the
# CERN OHL v.1.2. (http:#ohwr.org/cernohl). This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
# the CERN OHL v.1.2 for applicable conditions.

# Directories
source set_variables.tcl

# Restore deisgn
if { [file exist "$projectDir/caribou-soc.xpr"] } {
    open_project "$projectDir/caribou-soc.xpr"
    
} else {
    source restore_project.tcl
}

generate_target all [get_files  "$ipiDir/caribou_top/caribou_top.bd"]

# Run synthesis
reset_run synth_1
launch_runs synth_1 -jobs $multiCore
wait_on_run synth_1
puts "Synthesis done !"


# Run implementation
reset_run impl_1
launch_runs impl_1 -jobs $multiCore
wait_on_run impl_1
puts "Implementation done !"

open_impl_design

# Reports
report_route_status -file $outputDir/post_route_status.rpt
report_timing_summary -file $outputDir/post_route_timing_summary.rpt
report_power -file $outputDir/post_route_power.rpt
report_drc -file $outputDir/post_imp_drc.rpt

# Write bitstream
#set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]
write_bitstream -force $outputDir/caribou-soc.bit
write_debug_probes -force $outputDir/debug_probes.ltx
puts "Bitfile generated !"

close_design

# Write hadware definition
write_hwdef -force  -file $outputDir/caribou_top.hdf
puts "Hardware definition created !"

#Generate device tree files (dts, dtsi)
#exec hsi -nojournal -nolog -source generate_device_tree.tcl
#oputs "Device tree files generated !"


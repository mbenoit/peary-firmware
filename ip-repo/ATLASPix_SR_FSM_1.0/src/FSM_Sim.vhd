----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/16/2017 01:44:33 PM
-- Design Name: 
-- Module Name: FSM_Sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSM_Sim is
--  Port ( );
end FSM_Sim;

architecture Behavioral of FSM_Sim is


	signal ccpd_cfg_flg :std_logic;
    signal ccpd_cfg_reg_limit :std_logic_vector(5 downto 0);
    signal ccpd_cfg_shift_limit :std_logic_vector(4 downto 0);
    signal ccpd_cfg_clk_en :std_logic_vector(1 downto 0);
    signal ccpd_cfg_ram_wr_en :std_logic;
    signal ccpd_ram_wr_dat :std_logic_vector(31 downto 0);
    signal ccpd_cfg_ram_addr :std_logic_vector(5 downto 0);
    signal ccpd_cfg_ram_rd_dat :std_logic_vector(31 downto 0);
    signal ccpd_cfg_out_en :std_logic_vector(3 downto 0);
    
    signal atlaspix_sin      :std_logic_vector(2 downto 0); -- H35DEMO config. SIN
    signal atlaspix_ck1      :std_logic_vector(2 downto 0);-- H35DEMO config. CK1
    signal atlaspix_ck2      :std_logic_vector(2 downto 0); -- H35DEMO config. CK2
    signal atlaspix_ldM1      :std_logic; -- H35DEMO config. LD_nMOS
    signal atlaspix_ldM1ISO      :std_logic; -- H35demo config. LD_ANA1
    signal atlaspix_ldM2      :std_logic; -- H35DEMO config. LD_ANA2
    signal atlaspix_ldSpare      :std_logic; -- H18V4 config. LD_H18V4
    signal atlaspix_ldSpare2      :std_logic; -- H35DEMO config. LD_CMOS
    signal clk      :std_logic; -- H35DEMO config. LD_CMOS

begin

h35demo_cfg:entity xil_defaultlib.h35demo_cfg_wrapper
    generic map( 
        input_clk_fre  => 100_000_000, --input clock speed from user logic in Hz
        cfg_clk_freq   =>  125_000  --frequency of the Ck1 and Ck2 in Hz
    )
    Port map (
        CLK              => CLK,
        RST              => global_reset,
    
        START_FLG        => ccpd_cfg_flg,
        REG_LIMIT        => ccpd_cfg_reg_limit,
        SHIFT_LIMIT      => ccpd_cfg_shift_limit,
        OUT_EN           => ccpd_cfg_out_en,
    
        RAM_WR_EN        => ccpd_cfg_ram_wr_en,
        RAM_WR_DAT       => ccpd_ram_wr_dat,
        RAM_ADDR         => ccpd_cfg_ram_addr,
        RAM_RD_DAT       => ccpd_cfg_ram_rd_dat,
        RAM_WR_CLK       => CLK,
               
        SIN              => atlaspix_sin,
        CK1              => atlaspix_ck1,
        CK2              => atlaspix_ck2,
        LD_NMOS          => atlaspix_ldM1,
        LD_ANA1          => atlaspix_ldM1ISO,
        LD_ANA2          => atlaspix_ldM2,
        LD_CMOS          => atlaspix_ldSpare
     );
     
     
     clk <= not clk after PERIOD/2;
     ccpd_cfg_out_en <= '1111'
     stim_proc : process
     begin
        ccpd_ram_wr_dat <= 'AAAA';
        wait for 100 ns;
        RAM_ADDR <= '000000';
        wait for 100 ns; 
        ccpd_ram_wr_dat <= 'BBBB';
        wait for 100 ns;
        RAM_ADDR <= '000001';
        wait for 100 ns;         
        ccpd_ram_wr_dat <= 'CCCC';
        wait for 100 ns;
        RAM_ADDR <= '000010';
        wait for 100 ns;
        ccpd_cfg_reg_limit <= '000010'
        ccpd_cfg_shift_limit <= '00000'
        wait for 100 ns;
        ccpd_cfg_flg <= '1';
        wait for 2000 ns;
     end process;
    
end Behavioral;

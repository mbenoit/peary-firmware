//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_receiver_Package.sv
// Description     : CLICpix2_receiver_Package.sv
// Author          : Adrian Fiergolski
// Created On      : Mon Apr 24 15:09:32 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef CLICPIC2_RECEIVER_PACKAGE
 `define CLICPIC2_RECEIVER_PACKAGE

`include "UtilitiesPackage.sv"

//Package CLICpix2_receiver_Package
package CLICpix2_receiver_Package;

   import UtilitiesPackage::*;

   export UtilitiesPackage::CLOCK_DOMAIN;


endpackage // CLICpix2_receiver_Package

`endif

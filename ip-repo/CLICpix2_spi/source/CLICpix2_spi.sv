//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_spi.sv
// Description     : Custom SPI block which generates SPI clock outside SPI transactions.
// Author          : Adrian Fiergolski
// Created On      : Tue Jul  4 18:49:24 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`timescale 1ns / 1ps

module CLICpix2_spi  #(AXI_DATA_WIDTH = 32, AXI_ADDR_WIDTH = 7, USE_CHIPSCOPE = 1)
  (
   //Fast clock used internally by the module
   input logic 				  ext_spi_clk,

   //Slow clock (2 times slower than ext_spi_clk) used as SPI clock
   //This clock is send to CLICpix2
   input logic 				  CLICpix2_slow_clock,

   //SPI
   output logic 			  CLICpix2_ss_p,
   output logic 			  CLICpix2_ss_n,
   output logic 			  CLICpix2_mosi_p,
   output logic 			  CLICpix2_mosi_n,
   input logic 				  CLICpix2_miso_p,
   input logic 				  CLICpix2_miso_n,
   //Clock is provided directly by the CaR clock generator
   //output logic 			  CLICpix2_slow_clock_p,
   //output logic 			  CLICpix2_slow_clock_n,
   
   ///////////////////////
   //AXI4-Lite interface
   ///////////////////////

    //Write address channel
   input logic [AXI_ADDR_WIDTH-1 : 0] 	  awaddr,
   input logic [2 : 0] 			  awprot,
   input logic 				  awvalid,
   output logic 			  awready,

    //Write data channel
   input logic [AXI_DATA_WIDTH-1 : 0] 	  wdata,
   input logic [(AXI_DATA_WIDTH/8)-1 : 0] wstrb,
   input logic 				  wvalid,
   output logic 			  wready,
   
    //Write response channel
   output logic [1 : 0] 		  bresp,
   output logic 			  bvalid,
   input logic 				  bready,

    //Read address channel
   input logic [AXI_ADDR_WIDTH-1 : 0] 	  araddr,
   input logic [2 : 0] 			  arprot,
   input logic 				  arvalid,
   output logic 			  arready,

    //Read data channel
   output logic [AXI_DATA_WIDTH-1 : 0] 	  rdata,
   output logic [1 : 0] 		  rresp,
   output logic 			  rvalid,
   input logic 				  rready,
   
   input logic 				  aclk,
   input logic 				  aresetN,

   output logic 			  ip2intc_irpt
   );

   logic 				  CLICpix2_ss;
   logic 				  CLICpix2_mosi;
   logic 				  CLICpix2_miso;
   logic                                  core_sck;
	 
   
   axi_quad_spi_0 spi( .ext_spi_clk(ext_spi_clk),
		       .clicpix2_slow_clock(CLICpix2_slow_clock),
		       
		       //AXI Lite
		       .s_axi_aclk(aclk), .s_axi_aresetn(aresetN),
		       .s_axi_awaddr(awaddr), .s_axi_awvalid(awvalid),
		       .s_axi_awready(awready), 
		       .s_axi_wdata(wdata), .s_axi_wstrb(wstrb),
		       .s_axi_wvalid(wvalid), .s_axi_wready(wready),
		       .s_axi_bresp(bresp), .s_axi_bvalid(bvalid),
		       .s_axi_bready(bready),
		       .s_axi_araddr(araddr), .s_axi_arvalid(arvalid),
		       .s_axi_arready(arready), 
		       .s_axi_rdata(rdata), .s_axi_rresp(rresp),
		       .s_axi_rvalid(rvalid), .s_axi_rready(rready),

		       //SPI
		       .io0_i(1'b0),
		       .io0_o(CLICpix2_mosi),
		       .io0_t(),
		       .io1_i(CLICpix2_miso),
		       .io1_o(),
		       .io1_t(),
		       .sck_i(1'b0),
		       .sck_o(core_sck),
		       .sck_t(),
		       .ss_i(1'b0),
		       .ss_o(CLICpix2_ss),
		       .ss_t(),
		       .ip2intc_irpt(ip2intc_irpt) );

   //LVDS buffers
   
   OBUFDS #(
       .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
       .SLEW("SLOW")           // Specify the output slew rate
    ) OBUFDS_CLICpix2_SS_inst (
       .O(CLICpix2_ss_p),     // Diff_p output (connect directly to top-level port)
       .OB(CLICpix2_ss_n),   // Diff_n output (connect directly to top-level port)
       .I(CLICpix2_ss)      // Buffer input 
    );
    
    OBUFDS #(
       .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
       .SLEW("SLOW")           // Specify the output slew rate
    ) OBUFDS_CLICpix2_MOSI_inst (
       .O(CLICpix2_mosi_p),     // Diff_p output (connect directly to top-level port)
       .OB(CLICpix2_mosi_n),   // Diff_n output (connect directly to top-level port)
       .I(CLICpix2_mosi)      // Buffer input 
    );
   
/* -----\/----- EXCLUDED -----\/-----
    ODDR #(
        .DDR_CLK_EDGE("SAME_EDGE"), // "OPPOSITE_EDGE" or "SAME_EDGE" 
        .INIT(1'b0),    // Initial value of Q: 1'b0 or 1'b1
        .SRTYPE("SYNC") // Set/Reset type: "SYNC" or "ASYNC" 
    ) ODDR_inst (
        .Q(CLICpix2_slow_clock_oddr),   // 1-bit DDR output
        .C(CLICpix2_slow_clock),   // 1-bit clock input
        .CE(1'b1), // 1-bit clock enable input
        .D1(1'b1), // 1-bit data input (positive edge)
        .D2(1'b0), // 1-bit data input (negative edge)
        .R(1'b0),   // 1-bit reset
        .S(1'b0)    // 1-bit set
    );
 -----/\----- EXCLUDED -----/\----- */
   
    OBUFDS #(
       .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
       .SLEW("SLOW")           // Specify the output slew rate
    ) OBUFDS_CLICpix2_MISO_inst (
       .O(CLICpix2_slow_clock_p),     // Diff_p output (connect directly to top-level port)
       .OB(CLICpix2_slow_clock_n),   // Diff_n output (connect directly to top-level port)
       .I(CLICpix2_slow_clock_oddr)      // Buffer input 
    );
   
    IBUFDS #(
       .DIFF_TERM("TRUE"),       // Differential Termination
       .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE" 
       .IOSTANDARD("LVDS_25")     // Specify the input I/O standard
    ) IBUFDS_CLICpix2_MISO_inst (
       .O(CLICpix2_miso),  // Buffer output
       .I(CLICpix2_miso_p),  // Diff_p buffer input (connect directly to top-level port)
       .IB(CLICpix2_miso_n) // Diff_n buffer input (connect directly to top-level port)
    );

/* -----\/----- EXCLUDED -----\/-----
   always_ff @(posedge CLICpix2_slow_clock_miso) begin : LATCH_OF_MISO_PIN
      CLICpix2_miso <= CLICpix2_miso_i;
   end
 -----/\----- EXCLUDED -----/\----- */

   /////////////////
   // Debugging
   /////////////////
   if( USE_CHIPSCOPE ) begin : chipscope
      //pins
      (*mark_debug = "TRUE" *) logic ss;
      (*mark_debug = "TRUE" *) logic mosi;
      (*mark_debug = "TRUE" *) logic miso;
      (*mark_debug = "TRUE" *) logic slow_clock;
      (*mark_debug = "TRUE" *) logic sck;

      
      assign ss = CLICpix2_ss;
      assign mosi = CLICpix2_mosi;
      assign miso = CLICpix2_miso;
      assign slow_clock = CLICpix2_slow_clock;
      assign sck = core_sck;
      
/* -----\/----- EXCLUDED -----\/-----
      always_ff @(posedge ext_spi_clk) begin : ALIGN_SPI_OUTPUTS_WITH_INPUTS_ON_WAVEFORM
	 logic ss_d;
	 logic mosi_d;
	 ss <= ss_d;
	 mosi <= mosi_d;
	 ss_d <= CLICpix2_ss;
	 mosi_d <= CLICpix2_mosi;
	 miso <= CLICpix2_miso;
      end
 -----/\----- EXCLUDED -----/\----- */
      
      CLICpix2_spi_ila monitor
	( .clk(ext_spi_clk),
	  .probe0(ss),
	  .probe1(mosi),
	  .probe2(miso),
	  .probe3(slow_clock),
	  .probe4(sck) );
   end

endmodule

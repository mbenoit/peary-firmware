//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_control.sv
// Description     : The block controls CLICpix2 pins.
// Author          : Adrian Fiergolski
// Created On      : Tue May  2 14:21:28 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`timescale 1ns / 1ps

module CLICpix2_control  #(AXI_DATA_WIDTH = 32, AXI_ADDR_WIDTH = 8, WAVE_CONTROL_EVENTS_NUMBER = 32, USE_CHIPSCOPE = 1)
   (    

        output logic 			       CLICpix2_reset_p, CLICpix2_reset_n,
	output logic 			       CLICpix2_pwr_pulse_p, CLICpix2_pwr_pulse_n,
	output logic 			       CLICpix2_shutter_p, CLICpix2_shutter_n,
	output logic 			       CLICpix2_tp_sw_p, CLICpix2_tp_sw_n,

	input logic 			       CLICpix2_slow_clock, 

	output logic 			       C3PD_reset_p, C3PD_reset_n,

	//TLU interface
	input wire 			       TLU_clk,
	input wire 			       TLU_trigger_p,
	input wire 			       TLU_trigger_n,
	output wire 			       TLU_busy_p,
	output wire 			       TLU_busy_n,
	input wire 			       TLU_reset_p,
	input wire 			       TLU_reset_n,

	//Interrupt
	//It generates an interrupt (rising edge) on falling edge of the shutter.
	output logic 			       ip2intc_irpt,

    ///////////////////////
    //AXI4-Lite interface
    ///////////////////////

    //Write address channel
	input logic [AXI_ADDR_WIDTH-1 : 0]     awaddr,
	input logic [2 : 0] 		       awprot,
	input logic 			       awvalid,
	output logic 			       awready,

    //Write data channel
	input logic [AXI_DATA_WIDTH-1 : 0]     wdata,
	input logic [(AXI_DATA_WIDTH/8)-1 : 0] wstrb,
	input logic 			       wvalid,
	output logic 			       wready,
   
    //Write response channel
	output logic [1 : 0] 		       bresp,
	output logic 			       bvalid,
	input logic 			       bready,

    //Read address channel
	input logic [AXI_ADDR_WIDTH-1 : 0]     araddr,
	input logic [2 : 0] 		       arprot,
	input logic 			       arvalid,
	output logic 			       arready,

    //Read data channel
	output logic [AXI_DATA_WIDTH-1 : 0]    rdata,
	output logic [1 : 0] 		       rresp,
	output logic 			       rvalid,
	input logic 			       rready,
   
	input logic 			       aclk,
	input logic 			       aresetN

    );

   ///////////////////////
   // AXI Lite interface
   ///////////////////////

   AXI4LiteInterface  #(AXI_DATA_WIDTH, AXI_ADDR_WIDTH) axi(aclk, aresetN);
   //Write address channel
   assign axi.awaddr = awaddr;
   assign axi.awprot = awprot;
   assign axi.awvalid = awvalid;
   assign awready = axi.awready;

   //Write data channel
   assign axi.wdata = wdata;
   assign axi.wstrb = wstrb;
   assign axi.wvalid = wvalid;
   assign wready = axi.wready;
   
   //Write response channel
   assign bresp = axi.bresp;
   assign bvalid = axi.bvalid;
   assign axi.bready = bready;

   //Read address channel
   assign axi.araddr = araddr;
   assign axi.arprot = arprot;
   assign axi.arvalid = arvalid;
   assign arready = axi.arready;

   //Read data channel
   assign rdata = axi.rdata;
   assign rresp = axi.rresp;
   assign rvalid = axi.rvalid;
   assign axi.rready = rready;

   ////////////////////
   // Pins interfaces
   /////////////////////
   CLICpix2_pinsInterface CLICpix2_pins();
   C3PD_pinsInterface C3PD_pins();

   ////////////////
   // AXI FSM
   ////////////////
   CLICpix2_control_AXI axi_fsm( .* );

   //////////////////
   // Wave generator
   //////////////////
   logic waveControl_reset;
   always_ff @(posedge CLICpix2_slow_clock) begin : SYNCHORNIZE_RESET
      (* ASYNC_REG = "TRUE" *) logic waveControl_reset_d[1:0];
      
      waveControl_reset <= waveControl_reset_d[1];
      waveControl_reset_d[1] <= waveControl_reset_d[0];
      waveControl_reset_d[0] <= axi.aresetN;
   end
   
     
   CLICpix2_waveGeneratorControlInterface #(WAVE_CONTROL_EVENTS_NUMBER) waveControl(CLICpix2_slow_clock, waveControl_reset);
   CLICpix2_waveGenerator waveGenerator( .* );
   TLU_interface tlu();
   //Should be in waveGenerator interface
   //Vivado has an issue with nest interfaces
   FIFO_interface #(51) timestampsFIFO();


   /////////////////////////
   // Interrupts
   /////////////////////////
   assign ip2intc_irpt = ~ CLICpix2_pins.shutter;
   
   /////////////////////////
   //Differential outputs
   /////////////////////////

   OBUFDS #(
	    .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
	    .SLEW("SLOW")           // Specify the output slew rate
	    ) OBUFDS_CLICpix2_inst (
				    .O(CLICpix2_reset_p),     // Diff_p output (connect directly to top-level port)
				    .OB(CLICpix2_reset_n),   // Diff_n output (connect directly to top-level port)
				    .I( CLICpix2_pins.reset )      // Buffer input 
				    );

   OBUFDS #(
	    .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
	    .SLEW("SLOW")           // Specify the output slew rate
	    ) OBUFDS_CLICpix2_PWR_PULSE_inst (
					      .O(CLICpix2_pwr_pulse_p),     // Diff_p output (connect directly to top-level port)
					      .OB(CLICpix2_pwr_pulse_n),   // Diff_n output (connect directly to top-level port)
					      .I( CLICpix2_pins.pwr_pulse )      // Buffer input 
					      );

   OBUFDS #(
	    .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
	    .SLEW("SLOW")           // Specify the output slew rate
	    ) OBUFDS_CLICpix2_SHUTTER_inst (
					    .O(CLICpix2_shutter_p),     // Diff_p output (connect directly to top-level port)
					    .OB(CLICpix2_shutter_n),   // Diff_n output (connect directly to top-level port)
					    .I( CLICpix2_pins.shutter )      // Buffer input 
					    );

   OBUFDS #(
	    .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
	    .SLEW("SLOW")           // Specify the output slew rate
	    ) OBUFDS_CLICpix2_TP_SW_inst (
					  .O(CLICpix2_tp_sw_p),     // Diff_p output (connect directly to top-level port)
					  .OB(CLICpix2_tp_sw_n),   // Diff_n output (connect directly to top-level port)
					  .I( CLICpix2_pins.tp_sw )      // Buffer input 
					  );      
   
   OBUFDS #(
	    .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
	    .SLEW("SLOW")           // Specify the output slew rate
	    ) OBUFDS_C3PD_inst (
				.O(C3PD_reset_p),     // Diff_p output (connect directly to top-level port)
				.OB(C3PD_reset_n),   // Diff_n output (connect directly to top-level port)
				.I( C3PD_pins.reset )      // Buffer input 
				);
   //TLU
   assign tlu.clk = TLU_clk;

/* -----\/----- EXCLUDED -----\/-----
   IBUFDS #(
	    .DIFF_TERM("TRUE"),       // Differential Termination
	    .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE" 
	    .IOSTANDARD("LVDS_25")     // Specify the input I/O standard
   ) IBUFDS_TLU_CLK_inst (
	    .O(tlu.clk),  // Buffer output
	    .I(TLU_clk_p),  // Diff_p buffer input (connect directly to top-level port)
	    .IB(TLU_clk_n) // Diff_n buffer input (connect directly to top-level port)
   );
 -----/\----- EXCLUDED -----/\----- */

   IBUFDS #(
	    .DIFF_TERM("TRUE"),       // Differential Termination
	    .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE" 
	    .IOSTANDARD("LVDS_25")     // Specify the input I/O standard
   ) IBUFDS_TLU_trigger_inst (
	    .O(tlu.trigger),  // Buffer output
	    .I(TLU_trigger_p),  // Diff_p buffer input (connect directly to top-level port)
	    .IB(TLU_trigger_n) // Diff_n buffer input (connect directly to top-level port)
   );

   OBUFDS #(
	    .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
	    .SLEW("SLOW")           // Specify the output slew rate
   ) OBUFDS_TLU_busy_inst (
	    .O(TLU_busy_p),     // Diff_p output (connect directly to top-level port)
	    .OB(TLU_busy_n),   // Diff_n output (connect directly to top-level port)
	    .I( tlu.busy )      // Buffer input 
   );

   IBUFDS #(
	    .DIFF_TERM("TRUE"),       // Differential Termination
	    .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE" 
	    .IOSTANDARD("LVDS_25")     // Specify the input I/O standard
   ) IBUFDS_TLU_reset_inst (
	    .O(tlu.reset),  // Buffer output
	    .I(TLU_reset_p),  // Diff_p buffer input (connect directly to top-level port)
	    .IB(TLU_reset_n) // Diff_n buffer input (connect directly to top-level port)
   );
   
   /////////////////
   // Debugging
   /////////////////
   if( USE_CHIPSCOPE ) begin : chipscope
      //pins
      (*mark_debug = "TRUE" *) logic tp_sw;
      (*mark_debug = "TRUE" *) logic shutter;
      (*mark_debug = "TRUE" *) logic pwr_pulse;
      (*mark_debug = "TRUE" *) logic reset;

      (*mark_debug = "TRUE" *) logic c3pd_reset;

      
      (*mark_debug = "TRUE" *) logic sequence_enable;
      (*mark_debug = "TRUE" *) logic sequence_loop;

      assign tp_sw = CLICpix2_pins.tp_sw;
      assign shutter = CLICpix2_pins.shutter;
      assign pwr_pulse = CLICpix2_pins.pwr_pulse;
      assign reset = CLICpix2_pins.reset;

      assign c3pd_reset = C3PD_pins.reset;

      assign sequence_enable = waveControl.en;
      assign sequence_loop = waveControl.loop;
      
      CLICpix2_control_ila monitor
	(.clk(CLICpix2_slow_clock),
	 .probe0(tp_sw),
	 .probe1(shutter),
	 .probe2(pwr_pulse),
	 .probe3(reset),
	 .probe4(c3pd_reset),
	 .probe5(sequence_enable),
	 .probe6(sequence_loop) );
   end
   
endmodule // CLICpix2_control

   

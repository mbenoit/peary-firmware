-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
-- Date        : Tue Sep 19 11:47:24 2017
-- Host        : pcunigefelix.dyndns.cern.ch running 64-bit Scientific Linux CERN SLC release 6.9 (Carbon)
-- Command     : write_vhdl -force -mode synth_stub -rename_top caribou_top_util_ds_buf_1_1 -prefix
--               caribou_top_util_ds_buf_1_1_ caribou_top_util_ds_buf_1_0_stub.vhdl
-- Design      : caribou_top_util_ds_buf_1_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z045ffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity caribou_top_util_ds_buf_1_1 is
  Port ( 
    OBUF_IN : in STD_LOGIC_VECTOR ( 2 downto 0 );
    OBUF_DS_P : out STD_LOGIC_VECTOR ( 2 downto 0 );
    OBUF_DS_N : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );

end caribou_top_util_ds_buf_1_1;

architecture stub of caribou_top_util_ds_buf_1_1 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "OBUF_IN[2:0],OBUF_DS_P[2:0],OBUF_DS_N[2:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "util_ds_buf,Vivado 2017.2";
begin
end;

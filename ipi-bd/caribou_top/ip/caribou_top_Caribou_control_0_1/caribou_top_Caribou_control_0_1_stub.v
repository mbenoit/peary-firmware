// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
// Date        : Tue Sep 19 11:47:25 2017
// Host        : pcunigefelix.dyndns.cern.ch running 64-bit Scientific Linux CERN SLC release 6.9 (Carbon)
// Command     : write_verilog -force -mode synth_stub
//               /home/telescope/Caribou-Peary/peary-firmware/ipi-bd/caribou_top/ip/caribou_top_Caribou_control_0_1/caribou_top_Caribou_control_0_1_stub.v
// Design      : caribou_top_Caribou_control_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z045ffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "Caribou_control,Vivado 2017.2" *)
module caribou_top_Caribou_control_0_1(awaddr, awprot, awvalid, awready, wdata, wstrb, 
  wvalid, wready, bresp, bvalid, bready, araddr, arprot, arvalid, arready, rdata, rresp, rvalid, rready, 
  aclk, aresetN)
/* synthesis syn_black_box black_box_pad_pin="awaddr[2:0],awprot[2:0],awvalid,awready,wdata[31:0],wstrb[3:0],wvalid,wready,bresp[1:0],bvalid,bready,araddr[2:0],arprot[2:0],arvalid,arready,rdata[31:0],rresp[1:0],rvalid,rready,aclk,aresetN" */;
  input [2:0]awaddr;
  input [2:0]awprot;
  input awvalid;
  output awready;
  input [31:0]wdata;
  input [3:0]wstrb;
  input wvalid;
  output wready;
  output [1:0]bresp;
  output bvalid;
  input bready;
  input [2:0]araddr;
  input [2:0]arprot;
  input arvalid;
  output arready;
  output [31:0]rdata;
  output [1:0]rresp;
  output rvalid;
  input rready;
  input aclk;
  input aresetN;
endmodule

// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
// Date        : Tue Sep 19 11:47:25 2017
// Host        : pcunigefelix.dyndns.cern.ch running 64-bit Scientific Linux CERN SLC release 6.9 (Carbon)
// Command     : write_verilog -force -mode funcsim
//               /home/telescope/Caribou-Peary/peary-firmware/ipi-bd/caribou_top/ip/caribou_top_Caribou_control_0_1/caribou_top_Caribou_control_0_1_sim_netlist.v
// Design      : caribou_top_Caribou_control_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z045ffg900-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "caribou_top_Caribou_control_0_1,Caribou_control,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "Caribou_control,Vivado 2017.2" *) 
(* NotValidForBitStream *)
module caribou_top_Caribou_control_0_1
   (awaddr,
    awprot,
    awvalid,
    awready,
    wdata,
    wstrb,
    wvalid,
    wready,
    bresp,
    bvalid,
    bready,
    araddr,
    arprot,
    arvalid,
    arready,
    rdata,
    rresp,
    rvalid,
    rready,
    aclk,
    aresetN);
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWADDR" *) input [2:0]awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWPROT" *) input [2:0]awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWVALID" *) input awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWREADY" *) output awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WDATA" *) input [31:0]wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WSTRB" *) input [3:0]wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WVALID" *) input wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WREADY" *) output wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BRESP" *) output [1:0]bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BVALID" *) output bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BREADY" *) input bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARADDR" *) input [2:0]araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARPROT" *) input [2:0]arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARVALID" *) input arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARREADY" *) output arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RDATA" *) output [31:0]rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RRESP" *) output [1:0]rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RVALID" *) output rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RREADY" *) input rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 aclk CLK" *) input aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 aresetN RST" *) input aresetN;

  wire aclk;
  wire [2:0]araddr;
  wire aresetN;
  wire [2:0]arprot;
  wire arready;
  wire arvalid;
  wire [2:0]awaddr;
  wire [2:0]awprot;
  wire awready;
  wire awvalid;
  wire bready;
  wire [1:0]bresp;
  wire bvalid;
  wire [31:0]rdata;
  wire rready;
  wire [1:0]rresp;
  wire rvalid;
  wire [31:0]wdata;
  wire wready;
  wire [3:0]wstrb;
  wire wvalid;

  (* AXI_ADDR_WIDTH = "3" *) 
  (* AXI_DATA_WIDTH = "32" *) 
  caribou_top_Caribou_control_0_1_Caribou_control inst
       (.aclk(aclk),
        .araddr(araddr),
        .aresetN(aresetN),
        .arprot(arprot),
        .arready(arready),
        .arvalid(arvalid),
        .awaddr(awaddr),
        .awprot(awprot),
        .awready(awready),
        .awvalid(awvalid),
        .bready(bready),
        .bresp(bresp),
        .bvalid(bvalid),
        .rdata(rdata),
        .rready(rready),
        .rresp(rresp),
        .rvalid(rvalid),
        .wdata(wdata),
        .wready(wready),
        .wstrb(wstrb),
        .wvalid(wvalid));
endmodule

(* AXI_ADDR_WIDTH = "3" *) (* AXI_DATA_WIDTH = "32" *) (* ORIG_REF_NAME = "Caribou_control" *) 
module caribou_top_Caribou_control_0_1_Caribou_control
   (awaddr,
    awprot,
    awvalid,
    awready,
    wdata,
    wstrb,
    wvalid,
    wready,
    bresp,
    bvalid,
    bready,
    araddr,
    arprot,
    arvalid,
    arready,
    rdata,
    rresp,
    rvalid,
    rready,
    aclk,
    aresetN);
  input [2:0]awaddr;
  input [2:0]awprot;
  input awvalid;
  output awready;
  input [31:0]wdata;
  input [3:0]wstrb;
  input wvalid;
  output wready;
  output [1:0]bresp;
  output bvalid;
  input bready;
  input [2:0]araddr;
  input [2:0]arprot;
  input arvalid;
  output arready;
  output [31:0]rdata;
  output [1:0]rresp;
  output rvalid;
  input rready;
  input aclk;
  input aresetN;

  wire \<const1> ;
  wire aclk;
  wire [2:0]araddr;
  wire aresetN;
  wire arvalid;
  wire awvalid;
  wire bready;
  wire [0:0]\^bresp ;
  wire bvalid;
  wire [31:0]rdata;
  wire rready;
  wire [1:1]\^rresp ;
  wire rvalid;
  wire wready;
  wire wvalid;

  assign arready = \<const1> ;
  assign awready = wready;
  assign bresp[1] = \^bresp [0];
  assign bresp[0] = \^bresp [0];
  assign rresp[1] = \^rresp [1];
  assign rresp[0] = \^rresp [1];
  VCC VCC
       (.P(\<const1> ));
  caribou_top_Caribou_control_0_1_Caribou_control_AXI axi_fsm
       (.aclk(aclk),
        .araddr(araddr[2]),
        .aresetN(aresetN),
        .arvalid(arvalid),
        .awvalid(awvalid),
        .bready(bready),
        .bresp(\^bresp ),
        .bvalid(bvalid),
        .rdata(rdata),
        .rready(rready),
        .rresp(\^rresp ),
        .rvalid(rvalid),
        .wready(wready),
        .wvalid(wvalid));
endmodule

(* ORIG_REF_NAME = "Caribou_control_AXI" *) 
module caribou_top_Caribou_control_0_1_Caribou_control_AXI
   (wready,
    bresp,
    rdata,
    bvalid,
    rvalid,
    rresp,
    awvalid,
    wvalid,
    araddr,
    arvalid,
    rready,
    aclk,
    aresetN,
    bready);
  output wready;
  output [0:0]bresp;
  output [31:0]rdata;
  output bvalid;
  output rvalid;
  output [0:0]rresp;
  input awvalid;
  input wvalid;
  input [0:0]araddr;
  input arvalid;
  input rready;
  input aclk;
  input aresetN;
  input bready;

  wire aclk;
  wire [0:0]araddr;
  wire aresetN;
  wire arvalid;
  wire awvalid;
  wire bready;
  wire [0:0]bresp;
  wire bvalid;
  wire [31:0]rdata;
  wire \rdata[31]_INST_0_i_1_n_0 ;
  wire [31:0]\registers[0][q] ;
  wire rready;
  wire [0:0]rresp;
  wire rvalid;
  wire [1:0]stateR;
  wire \stateR[0]_i_1_n_0 ;
  wire \stateR[1]_i_1_n_0 ;
  wire \stateR[1]_i_2_n_0 ;
  wire [1:1]stateW;
  wire \stateW[1]_i_1_n_0 ;
  wire wready;
  wire wvalid;
  wire NLW_firmware_version_CFGCLK_UNCONNECTED;
  wire NLW_firmware_version_DATAVALID_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h5540)) 
    \__3/i_ 
       (.I0(stateR[0]),
        .I1(araddr),
        .I2(arvalid),
        .I3(stateR[1]),
        .O(rresp));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \bresp[0]_INST_0 
       (.I0(stateW),
        .I1(wvalid),
        .I2(awvalid),
        .O(bresp));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    bvalid_INST_0
       (.I0(wvalid),
        .I1(awvalid),
        .I2(stateW),
        .O(bvalid));
  (* BOX_TYPE = "PRIMITIVE" *) 
  USR_ACCESSE2 firmware_version
       (.CFGCLK(NLW_firmware_version_CFGCLK_UNCONNECTED),
        .DATA(\registers[0][q] ),
        .DATAVALID(NLW_firmware_version_DATAVALID_UNCONNECTED));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[0]_INST_0 
       (.I0(\registers[0][q] [0]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[0]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[10]_INST_0 
       (.I0(\registers[0][q] [10]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[10]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[11]_INST_0 
       (.I0(\registers[0][q] [11]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[11]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[12]_INST_0 
       (.I0(\registers[0][q] [12]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[12]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[13]_INST_0 
       (.I0(\registers[0][q] [13]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[13]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[14]_INST_0 
       (.I0(\registers[0][q] [14]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[14]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[15]_INST_0 
       (.I0(\registers[0][q] [15]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[15]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[16]_INST_0 
       (.I0(\registers[0][q] [16]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[16]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[17]_INST_0 
       (.I0(\registers[0][q] [17]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[17]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[18]_INST_0 
       (.I0(\registers[0][q] [18]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[18]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[19]_INST_0 
       (.I0(\registers[0][q] [19]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[19]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[1]_INST_0 
       (.I0(\registers[0][q] [1]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[20]_INST_0 
       (.I0(\registers[0][q] [20]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[20]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[21]_INST_0 
       (.I0(\registers[0][q] [21]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[21]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[22]_INST_0 
       (.I0(\registers[0][q] [22]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[22]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[23]_INST_0 
       (.I0(\registers[0][q] [23]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[23]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[24]_INST_0 
       (.I0(\registers[0][q] [24]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[24]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[25]_INST_0 
       (.I0(\registers[0][q] [25]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[25]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[26]_INST_0 
       (.I0(\registers[0][q] [26]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[26]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[27]_INST_0 
       (.I0(\registers[0][q] [27]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[27]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[28]_INST_0 
       (.I0(\registers[0][q] [28]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[28]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[29]_INST_0 
       (.I0(\registers[0][q] [29]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[29]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[2]_INST_0 
       (.I0(\registers[0][q] [2]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[2]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[30]_INST_0 
       (.I0(\registers[0][q] [30]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[30]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[31]_INST_0 
       (.I0(\registers[0][q] [31]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[31]));
  LUT4 #(
    .INIT(16'h5504)) 
    \rdata[31]_INST_0_i_1 
       (.I0(stateR[1]),
        .I1(arvalid),
        .I2(araddr),
        .I3(stateR[0]),
        .O(\rdata[31]_INST_0_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[3]_INST_0 
       (.I0(\registers[0][q] [3]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[3]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[4]_INST_0 
       (.I0(\registers[0][q] [4]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[4]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[5]_INST_0 
       (.I0(\registers[0][q] [5]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[5]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[6]_INST_0 
       (.I0(\registers[0][q] [6]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[6]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[7]_INST_0 
       (.I0(\registers[0][q] [7]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[8]_INST_0 
       (.I0(\registers[0][q] [8]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[8]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rdata[9]_INST_0 
       (.I0(\registers[0][q] [9]),
        .I1(\rdata[31]_INST_0_i_1_n_0 ),
        .O(rdata[9]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h3E)) 
    rvalid_INST_0
       (.I0(arvalid),
        .I1(stateR[1]),
        .I2(stateR[0]),
        .O(rvalid));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hC0C0CDCC)) 
    \stateR[0]_i_1 
       (.I0(araddr),
        .I1(stateR[0]),
        .I2(stateR[1]),
        .I3(arvalid),
        .I4(rready),
        .O(\stateR[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hC0C0F2F0)) 
    \stateR[1]_i_1 
       (.I0(araddr),
        .I1(stateR[0]),
        .I2(stateR[1]),
        .I3(arvalid),
        .I4(rready),
        .O(\stateR[1]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \stateR[1]_i_2 
       (.I0(aresetN),
        .O(\stateR[1]_i_2_n_0 ));
  FDCE \stateR_reg[0] 
       (.C(aclk),
        .CE(1'b1),
        .CLR(\stateR[1]_i_2_n_0 ),
        .D(\stateR[0]_i_1_n_0 ),
        .Q(stateR[0]));
  FDCE \stateR_reg[1] 
       (.C(aclk),
        .CE(1'b1),
        .CLR(\stateR[1]_i_2_n_0 ),
        .D(\stateR[1]_i_1_n_0 ),
        .Q(stateR[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h5540)) 
    \stateW[1]_i_1 
       (.I0(bready),
        .I1(wvalid),
        .I2(awvalid),
        .I3(stateW),
        .O(\stateW[1]_i_1_n_0 ));
  FDCE \stateW_reg[1] 
       (.C(aclk),
        .CE(1'b1),
        .CLR(\stateR[1]_i_2_n_0 ),
        .D(\stateW[1]_i_1_n_0 ),
        .Q(stateW));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h08)) 
    wready_INST_0
       (.I0(awvalid),
        .I1(wvalid),
        .I2(stateW),
        .O(wready));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
